Slack API:
=========

This repository is responsible for providing API for slack.  
If you are not familira with *Slack* go to official [slask site](https://slack.com/lp/one?cvosrc=ppc.google.%2Bslack&cvo_campaign=&cvo_campaign=&cvo_crid=213171048238&Matchtype=b&utm_source=&utm_medium=google&utm_campaign=brand_hv&c3api=5542,213171048238,%2Bslack&c3api=5542,213171048238,%2Bslack&cvosrc=ppc.google.%2Bslack&cvo_campaign=&cvo_crid=213171048238&Matchtype=b&utm_source=google&utm_medium=ppc&c3api=5523,213171048238,%2Bslack&gclid=EAIaIQobChMIq7WsyNT-1QIVDIuyCh21oAhhEAAYASAAEgJi0fD_BwE&gclsrc=aw.ds&dclid=CPjN9snU_tUCFRQOGQodyW8CJw)
to get know it.

Trello: [slack-api board](https://trello.com/b/42VkmfaX/slack-api)

Instalattion:
------------
1. Make copy of .env.example to .env

```bash
cp .env.example to .env
```

2. Install composer dependencies:

```bash
composer install
```

3. Create database and put credentials into .env

4. Run migrations:
```bash
php artisan migrate
```

API Requirements:
-----------------

use json format for input and output hence make *Content-Type* header required for json body.
